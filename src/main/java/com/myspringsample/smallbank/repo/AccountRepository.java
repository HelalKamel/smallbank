package com.myspringsample.smallbank.repo;

import com.myspringsample.smallbank.domain.Account;
import org.springframework.data.repository.CrudRepository;

public interface AccountRepository extends CrudRepository<Account,Integer>
{
    Account findByAccountNumber(Integer accountNumber);
}
