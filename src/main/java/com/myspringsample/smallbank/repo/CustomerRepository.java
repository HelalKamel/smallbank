package com.myspringsample.smallbank.repo;

import com.myspringsample.smallbank.domain.Customer;
import org.springframework.data.repository.CrudRepository;

public interface CustomerRepository extends CrudRepository<Customer,Integer>
{
    Customer findByCustomerName(String customerName);
}
