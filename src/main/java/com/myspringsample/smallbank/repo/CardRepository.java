package com.myspringsample.smallbank.repo;

import com.myspringsample.smallbank.domain.Card;
import org.springframework.data.repository.CrudRepository;


public interface CardRepository extends CrudRepository<Card,Integer>
{
    Card findByCardNumber(Integer cardNumber);
}
