package com.myspringsample.smallbank;

import com.myspringsample.smallbank.domain.Account;
import com.myspringsample.smallbank.domain.Customer;
import com.myspringsample.smallbank.repo.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import com.myspringsample.smallbank.services.AccountService;
import com.myspringsample.smallbank.services.CardService;
import com.myspringsample.smallbank.services.CustomerService;

@SpringBootApplication
public class SmallbankApplication implements CommandLineRunner
{

	@Autowired
	private CustomerService customer;

	@Autowired
	private CardService card;

	@Autowired
	private AccountService account;


	public static void main(String[] args) {
		SpringApplication.run(SmallbankApplication.class, args);
	}

	@Override
	public void run(String... args)
	{

		customer.createCustomer(1,"Ahmed Mohamed Helal","Egypt , Cairo");
		customer.createCustomer(2,"Konstantin Vorobyev","Germany , Berlin");
		customer.createCustomer(3,"Farida Ahmed Mohamed","Egypt , Cairo");


		account.createAccount(1234,1564.53,"Ahmed Mohamed Helal");
		account.createAccount(2546,1564.53,"Ahmed Mohamed Helal");
		account.createAccount(53423,15224.53,"Konstantin Vorobyev");
		account.createAccount(11211,5515224.53,"Farida Ahmed Mohamed");
		System.out.println(account.total());
		for (Account x: account.lookup()
		) {
			System.out.println(x.getAccountId());
			System.out.println(x.getAccountNumber());
			System.out.println(x.getCustomerId());
		}

		card.createCard(494608,1234,"Normal");
		card.createCard(544111,53423,"Normal");
		card.createCard(494607,11211,"Stolen");

	}
}
