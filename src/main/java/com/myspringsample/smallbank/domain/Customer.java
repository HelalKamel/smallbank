package com.myspringsample.smallbank.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Customer
{
    @Id
    private Integer id;

    @Column(length = 100)
    private String customerName;

    @Column(length = 100)
    private String customerAddress;

    public Customer()
    {
    }

    public Customer(Integer id , String customerName, String customerAddress)
    {
        this.customerName = customerName;
        this.customerAddress = customerAddress;
        this.id = id;
    }

    public Integer getId()
    {
        return id;
    }

    public String getCustomerName()
    {
        return customerName;
    }

    public void setCustomerName(String customerName)
    {
        this.customerName = customerName;
    }

    public String getCustomerAddress()
    {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress)
    {
        this.customerAddress = customerAddress;
    }

    @Override
    public String toString()
    {
        return "Customer{" +
                "name='" + customerName + '\'' +
                ", Address='" + customerAddress + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return Objects.equals(id, customer.id) &&
                Objects.equals(customerName, customer.customerName) &&
                Objects.equals(customerAddress, customer.customerAddress);

    }

    @Override
    public int hashCode()
    {
        return Objects.hash(id,customerName, customerAddress);
    }
}
