package com.myspringsample.smallbank.domain;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Card
{
    @Id
    @GeneratedValue
    private Integer cardId;

    @Column
    private Integer cardNumber;

    @Column(length = 100)
    private String cardStatus;

    @ManyToOne
    private Account accountId;

    public Integer getcardNumber()
    {
        return cardNumber;
    }

    public  Card()
    {

    }
    public Card(Integer cardNumber, String cardStatus, Account accountId)
    {
        this.cardNumber = cardNumber;
        this.cardStatus = cardStatus;
        this.accountId = accountId;
    }

    public Integer getCardId()
    {
        return cardId;
    }

    public void setcardNumber(Integer cardNumber)
    {
        this.cardNumber = cardNumber;
    }

    public String getCardStatus()
    {
        return cardStatus;
    }

    public void setCardStatus(String cardStatus)
    {
        this.cardStatus = cardStatus;
    }

    public Account getAccountId()
    {
        return accountId;
    }

    public void setAccountId(Account accountId)
    {
        this.accountId = accountId;
    }

    @Override
    public String toString() {
        return "Card{" +
                "CardId=" + cardId +
                ", cardNumber='" + cardNumber + '\'' +
                ", AccountId='" + accountId + '\'' +
                ", CardStatus='" + cardStatus+ '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Card card = (Card) o;
        return Objects.equals(cardId, card.cardId) &&
                Objects.equals(cardNumber, card.cardNumber) &&
                Objects.equals(cardStatus, card.cardStatus) &&
                Objects.equals(accountId, card.accountId);

    }

    @Override
    public int hashCode() {
        return Objects.hash(cardId,cardNumber,cardStatus,accountId);
    }
}
