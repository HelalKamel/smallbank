package com.myspringsample.smallbank.domain;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Account
{
    @Id
    @GeneratedValue
    private Integer accountId;

    @Column
    private Integer accountNumber;

    @Column
    private double balanace;

    @ManyToOne
    private Customer customerId;

    public Integer getAccountId()
    {
        return accountId;
    }

    public Account()
    {

    }
    public Account(Integer accountNumber, double balanace, Customer customerId)
    {
        this.accountNumber = accountNumber;
        this.balanace = balanace;
        this.customerId = customerId;
    }

    public Integer getAccountNumber()
    {
        return accountNumber;
    }

    public void setAccountNumber(Integer accountNumber)
    {
        this.accountNumber = accountNumber;
    }

    public double getBalanace()
    {
        return balanace;
    }

    public void setBalanace(double balanace)
    {
        this.balanace = balanace;
    }

    public Customer getCustomerId()
    {
        return customerId;
    }

    public void setCustomerId(Customer customerId)
    {
        this.customerId = customerId;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + accountId +
                ", AccountNumber='" + accountNumber + '\'' +
                ", CustomerId='" + customerId + '\'' +
                ", Balanace='" + balanace + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return Objects.equals(accountId, account.accountId) &&
                Objects.equals(accountNumber, account.accountNumber) &&
                Objects.equals(balanace, account.balanace) &&
                Objects.equals(customerId, account.customerId) ;
    }

    @Override
    public int hashCode() {
        return Objects.hash(accountId,accountNumber,balanace,customerId);
    }
}
