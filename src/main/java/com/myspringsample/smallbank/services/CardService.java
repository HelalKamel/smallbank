package com.myspringsample.smallbank.services;

import com.myspringsample.smallbank.domain.Account;
import com.myspringsample.smallbank.domain.Card;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.myspringsample.smallbank.repo.AccountRepository;
import com.myspringsample.smallbank.repo.CardRepository;

@Service
public class CardService
{
    private CardRepository cardRepository;
    private AccountRepository accountRepository;

    @Autowired
    public CardService(CardRepository cardRepository, AccountRepository accountRepository)
    {
        this.cardRepository = cardRepository;
        this.accountRepository = accountRepository;
    }

    public Card createCard(Integer cardNumber, Integer accountNumber , String cardStatus)
    {
        Account act = accountRepository.findByAccountNumber(accountNumber);

        if(act == null)
        {
            throw new RuntimeException("No Such Customer " + accountNumber);
        }
        else {
            return cardRepository.save(new Card(cardNumber, cardStatus,act));
        }
    }

    public Card getCardRecord(Integer cardNumber)
    {
        return cardRepository.findByCardNumber(cardNumber);
    }

    public Iterable<Card> lookup(){
        return cardRepository.findAll();
    }

    public long total(){
        return cardRepository.count();
    }
}
