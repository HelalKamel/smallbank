package com.myspringsample.smallbank.services;

import com.myspringsample.smallbank.domain.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.myspringsample.smallbank.repo.CustomerRepository;

@Service
public class CustomerService
{
    private CustomerRepository customerRepository;

    @Autowired
    public CustomerService(CustomerRepository customerRepository)
    {
        this.customerRepository = customerRepository;
    }

    public Customer createCustomer(Integer id , String customerName,String customerAddress)
    {
        return customerRepository.save(new Customer(id,customerName,customerAddress));
    }

//    public Customer getCustomer(String name)
//    {
//        return customerRepository.findByCustomerName(name);
//    }

    public Iterable<Customer> lookup(){
        return customerRepository.findAll();
    }

    public long total(){
        return customerRepository.count();
    }
}
