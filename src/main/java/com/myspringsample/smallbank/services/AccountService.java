package com.myspringsample.smallbank.services;

import com.myspringsample.smallbank.domain.Account;
import com.myspringsample.smallbank.domain.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.myspringsample.smallbank.repo.AccountRepository;
import com.myspringsample.smallbank.repo.CustomerRepository;

@Service
public class AccountService
{
    private AccountRepository accountRepository;
    private CustomerRepository customerRepository;

    @Autowired
    public AccountService(AccountRepository accountRepository, CustomerRepository customerRepository)
    {
        this.accountRepository = accountRepository;
        this.customerRepository = customerRepository;
    }

    public Account createAccount(Integer accountNumber, double Balanace , String customerName ){
        Customer cst = customerRepository.findByCustomerName(customerName);

        if(cst == null)
        {
            throw new RuntimeException("No Such Customer " + cst.getId());
        }
        else {
            return accountRepository.save(new Account(accountNumber, Balanace, cst));
        }
    }

    public Iterable<Account> lookup()
    {
        return accountRepository.findAll();
    }

    public long total(){
        return accountRepository.count();
    }
}
